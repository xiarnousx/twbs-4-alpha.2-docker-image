# Download from base image
FROM debian

# Update and install required packages
RUN apt-get -qq update \
&&  apt-get install -y curl  \
&& curl -sL https://deb.nodesource.com/setup_6.x | bash - \
&& apt-get install -y build-essential \
&& apt-get install -y nodejs \
&& apt-get install -f \
&& apt-get install --fix-missing 

# Create a /bootsrap working dir in order to download bootstrap 4 source    
WORKDIR /bootstrap

# Copy ./compiler.sh from host to container root
COPY ./bootstrap.sh /bootstrap.sh

# Attach volume between host and container
VOLUME ["/bootstrap"]

# Default command that compile bootstrap 4 alpha 2 into /bootstrap/dist dir
CMD ["/bootstrap.sh"]
