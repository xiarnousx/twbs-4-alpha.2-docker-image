#!/bin/bash
echo "npm install bootstrap@4.0.0-alpha.2" | /bin/bash -C
echo "mkdir -p /bootstrap/twbs4a/css /bootstrap/twbs4a/js" | /bin/bash -C
echo "mv /bootstrap/node_modules/bootstrap/dist/css/*.css /bootstrap/twbs4a/css" | /bin/bash -C
echo "mv /bootstrap/node_modules/bootstrap/dist/js/*.js /bootstrap/twbs4a/js" | /bin/bash -C
echo "chmod 777 -R /bootstrap" | /bin/bash -C
echo "rm /bootstrap/twbs4a/js/npm.js" | /bin/bash -C
echo "rm -rf /bootstrap/node_modules" | /bin/bash -C

