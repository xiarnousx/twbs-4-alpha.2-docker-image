# Downloading and Extracting Bootstrap 4 alpha.2

This docker file recipe is based on debian official image, it download and install nodejs using package mgmt.

Once all necessary packages are installed into the image, I inject into the image bootstrap.sh which basically download bootstrap alpha via node package mgmt npm do some clean up and publishes the extracted js and css files to bind mount volume.

## Instruction on Usage
```bash
docker build -t twbs4 .
docker run -v ${PWD}/bootstrap:/bootstrap twbs4
```
